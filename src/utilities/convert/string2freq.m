function freq = string2freq(s) % --*-- Unitary tests --*--

% INPUTS
%  o s        character, equal to Y, H, Q, M, W or D (resp. annual, bi-annual, quaterly, monthly, weekly or daily)
%
% OUTPUTS
%  o freq     scalar integer,  equal to 1, 2, 4, 12, 52 or 365 (resp. annual, bi-annual, quaterly, monthly, weekly or daily)

% Copyright (C) 2013-2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

switch upper(s)
  case {'Y','A'}
    freq = 1;
  case 'H'
    freq = 2;
  case 'Q'
    freq = 4;
  case 'M'
    freq = 12;
  case 'W'
    freq = 52;
  case 'D'
    freq = 365;
  otherwise
    error('dates::freq2string: Unknown frequency!')
end

%@test:1
%$ try
%$     nY = string2freq('Y');
%$     nH = string2freq('H');
%$     nQ = string2freq('Q');
%$     nM = string2freq('M');
%$     nW = string2freq('W');
%$     nD = string2freq('D');
%$     t(1) = true;
%$ catch
%$     t(1) = false;
%$ end
%$
%$ if t(1)
%$     t(2) = dassert(nY, 1);
%$     t(3) = dassert(nH, 2);
%$     t(4) = dassert(nQ, 4);
%$     t(5) = dassert(nM, 12);
%$     t(6) = dassert(nW, 52);
%$     t(7) = dassert(nD, 365);
%$ end
%$
%$ T = all(t);
%@eof:1

%@test:2
%$ try
%$     n = string2freq('Z');
%$     t(1) = false;
%$ catch
%$     t(1) = true;
%$ end
%$
%$ T = all(t);
%@eof:2
