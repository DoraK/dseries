function b = isdaily(str)  % --*-- Unitary tests --*--

% Tests if the input can be interpreted as a daily date.
%
% INPUTS
%  o str     string.
%
% OUTPUTS
%  o b       integer scalar, equal to 1 if str can be interpreted as a daily date or 0 otherwise.

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

if ischar(str)
    if isempty(regexp(str,'^-?[0-9]+[-](0[1-9]|1[0-2])+[-]([0-2][0-9]|3[0-1])$','once'))
        b = false;
    else
        b = true;
    end
else
    b = false;
end

%@test:1
%$
%$ date_1 = '1950-03-29';
%$ date_2 = '1950-12-31';
%$ date_3 = '-1950-11-28';
%$ date_4 = '1976-01-19';
%$ date_5 = '1950 azd ';
%$ date_6 = '1950Y';
%$ date_7 = '1950Q3';
%$ date_8 = '1950-13-39';
%$
%$ t(1) = dassert(isdaily(date_1),true);
%$ t(2) = dassert(isdaily(date_2),true);
%$ t(3) = dassert(isdaily(date_3),true);
%$ t(4) = dassert(isdaily(date_4),true);
%$ t(5) = dassert(isdaily(date_5),false);
%$ t(6) = dassert(isdaily(date_6),false);
%$ t(7) = dassert(isdaily(date_7),false);
%$ t(8) = dassert(isdaily(date_8),false);
%$ T = all(t);
%@eof:1